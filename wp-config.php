<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_web' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*U<Sgiu{Y(A^3|dBY]W.s+h*C9k]SSeQK&f]+$aP.=Uv)q.>jkrpnLday4/`-2Nm' );
define( 'SECURE_AUTH_KEY',  'd^1HC/&d2h!m)E+hC^?qaR[^M2~sv}!rWl]F[) ChrmJ_!V,1&:]p9zFRCCNIZi(' );
define( 'LOGGED_IN_KEY',    '3MW1[=3n-~@K?R`B#0:A<<U!*~^omcW>%(Sb-omy%*SIHl/9V`8*J5v08bHezYW.' );
define( 'NONCE_KEY',        ' QxyP{+coxvnGj6/q*+OkO~7 uT,6SvO_HQA!sd=v-E;3||]1k%:m#9o(C^3>%W?' );
define( 'AUTH_SALT',        'c !8~!/# y#WBvBWlgL2:!K#h{1L`/Y`#Z?cxPOG] M8pZcwche^62wSXboyb]Zw' );
define( 'SECURE_AUTH_SALT', '#P>VI^*ulig}r|S[jQU[3F~_lNQ@[nuN,*w#oo]>f5@ObG3JO.xW5HB)(1`,%G[l' );
define( 'LOGGED_IN_SALT',   ')vO+@GU+Z&PAb(ZQ~-srCYHh10dE+!^r@iuiR4dzM }sd6Rj-GnPh?y0Q71>I_^.' );
define( 'NONCE_SALT',       'F(~D{3A{G-Z97+PpYHBNv=#WwG$Hqrz$%?q^3==(+wd=?J }bURKt=H-&oR`Rf$Y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
